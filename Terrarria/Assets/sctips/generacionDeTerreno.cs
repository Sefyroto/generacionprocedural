﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generacionDeTerreno : MonoBehaviour
{
    public GameObject bloque;
    public int seed = 0;
    public float amp = 10f;
    public float freq = 10f;
    

    // Start is called before the first frame update
    void Start()
    {
        generateMap();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void generateMap()
    {
        Vector3 pos = this.transform.position;

        int fil = 50;
        int col = 50;
        for (int x = 0; x < col; x++)
        {
            for (int y=0; y< fil;y++)
            {
                float altura = Mathf.PerlinNoise((pos.x + x + seed)/freq, pos.y + y/freq);
                altura = altura * amp;
                //print(altura);

                if (altura > 25f)
                {
                    GameObject newBlock = Instantiate(bloque);
                    newBlock.transform.position = new Vector3(pos.x + x, pos.y + y);
                }
            }
        }
    }
}
