﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class g3dGeneracion : MonoBehaviour
{
    public float amplitud;
    public float frequencia;
    public float seedX;
    public float seedY;
    int ancho = 513;
    int alto = 513;
    public TerrainData tData;
    public int octavas;
    public float lacunaridad;
    public float persistencia;
    public Vector2 offset;
    public int seed;
    void Awake()
    {
        tData = generarMapa();
    }

    private void Update()
    {
        tData = generarMapa();
    }

    private TerrainData generarMapa()
    {
        tData.heightmapResolution = 513;
        tData.size = new Vector3(ancho,amplitud,alto);
        tData.SetHeights(0,0, generarAlturas());
        return tData;
    }

    private float[,] generarAlturas()
    {
        float[,] generarAlturas = new float[ancho,alto];
        Vector2[] ofsetOctavas = new Vector2[octavas];
        System.Random random = new System.Random(seed);

        for (int i = 0; i < octavas; i++)
        {
            Vector2 offsetReal = new Vector2();
            offsetReal.x = random.Next(-100000, 100000)+offset.x;
            offsetReal.y = random.Next(-100000, 100000) +offset.y;
            ofsetOctavas[i] = offsetReal;
        }
        if (frequencia <= 0)
        {
            frequencia = 0.0001f;
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = ancho / 2f;
        float halfHeight = alto / 2f;
        
        for (int x=0; ancho>x;x++)
        {
            for (int y = 0; alto > y; y++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHaigth = 0;

                for (int l = 0; l<octavas; l++)
                {
                    float sampleX = (x - halfWidth)/frequencia*frequency+ofsetOctavas[l].x;
                    float sampleY = (y - halfHeight)/frequencia*frequency+ofsetOctavas[l].y;
                    float perlingValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHaigth += perlingValue * amplitude;
                    amplitude *= persistencia;
                    frequency *= lacunaridad;
                }
                if (noiseHaigth > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHaigth;
                }else if (noiseHaigth < minNoiseHeight)
                {
                    minNoiseHeight = noiseHaigth;
                }
                generarAlturas[x, y] = noiseHaigth;
            }
        }
        for (int y = 0; y < ancho; y++)
        {
            for (int x = 0; x< alto; x++)
            {
                generarAlturas[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, generarAlturas[x,y]);
            }
        }
        return generarAlturas;
    }
}
